﻿using UIKit;
using Xamarin.Forms;
using NoffaPlus.Controls;
using NoffaPlus.iOS.Renderers;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace NoffaPlus.iOS.Renderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (this.Control != null)
                this.Control.BorderStyle = UITextBorderStyle.None;
        }
    }
}
