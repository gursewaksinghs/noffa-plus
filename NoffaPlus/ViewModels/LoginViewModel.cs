﻿using System.Threading.Tasks;
using Xamarin.Forms;
namespace NoffaPlus.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public LoginViewModel()
        {
        }

        #region Login Command.
        private Command loginCommand;
        public Command LoginCommand
        {
            get { return loginCommand ?? (loginCommand = new Command(async () => await ExecuteLoginCommand())); }
        }

        async Task ExecuteLoginCommand()
        {
            await Task.CompletedTask;
        }
        #endregion
    }
}
