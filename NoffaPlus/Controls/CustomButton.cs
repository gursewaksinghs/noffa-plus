﻿using Xamarin.Forms;

namespace NoffaPlus.Controls
{
    public class CustomButton : Button
    {
        public CustomButton()
        {
            TextColor = Color.Black;
            FontSize = 17;
            HeightRequest = 50;
            CornerRadius = 0;
            FontAttributes = FontAttributes.Bold;
            BackgroundColor = Color.FromHex("fbec00");
            HorizontalOptions = LayoutOptions.FillAndExpand;
        }
    }
}
