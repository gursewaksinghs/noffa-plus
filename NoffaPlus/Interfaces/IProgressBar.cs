﻿namespace NoffaPlus.Interfaces
{
    public interface IProgressBar
    {
        void ShowProgress();
        void Dismiss();
    }
}
